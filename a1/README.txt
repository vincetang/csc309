Created by:
  Vincent Tang 998192450
  g2tangvi

CSC309 A1 README
----------------

AMI ID: ami-b4e357dc

File locations:
/var/www/html/index.html
/var/www/html/basic.css

In case you need to revert to original files, copy of the files are stored here:
/persistent/csc309-master/a1/

Viewing webpage:
For desktop version
1. Open Mozilla Firefox
2. Type "localhost" in the address bar

For mobile version
1. Run Eclipse and open the AVD (Window -> Android Virtual Device Manager)
2. Start the device named "CustomAVD1", which runs a 980x550 resolution (however, the default Nexus 4 device will work too)
3. Run the browser app and type 10.0.2.2 in the address bar