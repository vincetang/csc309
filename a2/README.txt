CSC309 Assignment 2 - Breakout (HTML Canvas)
Vincent Tang
g2tangvi
998192450

AMI ID: ami-ccbe3ba4

Source Files: 
/var/www/html/index.html
/var/www/html/breakout.js
/var/www/html/basic.css
/var/www/html/background.png

No additional instructions for starting Apache

Recommended Browsers: Google Chrome, Mozilla Firefox

Documentation:
Main structures:
  > bricks[] - array of bricks (context rectangles) - global scope
  > brick - a brick object with properties such as score, color, and position coordinates (x,y)
  > ball - object with important properties like x position, y position and speed
  > paddle - object with important properties like x position, y position

How it works:
  1. loads up initial values and draws rows of bricks in order of color (14 bricks per row, 8 rows, 2 of each color)
  2. Initialize ball with velocity going down towards the paddle at an angle
  3. Below lists ball collisions and results (x and y refer to horizontal and verticle velocities of the ball, respectively):
      > canvas walls
          - bounce off and change x direction but not y direction
      > roof
        - bounce off and change y direction but not x direction
        - halve the width of the paddle
      > paddle
        - if it hits the flat top of the paddle, change y position and not x position
        - if it hits either cornor, change y position and x position appropriately to bounce off the sides of the paddle
      > bricks
        - if it hits the flat bottom of the brick, reverse y direction but not x
        - if it hits the cornors or side of the brick, bounce off the appropriate side
        - if it hits the top of the brick, do nothing to ball's velocity
        - all hits eliminate the brick which result in scores being added, potentially leveling up if all the bricks are destroyed
        - count hits to check for triggers to increase speed of ball
      > Bottom of canvas
        - travel out and lose a life
        - reset ball position if there are lives left
        - end game if no lives are left

How to play:
1. Click on the canvas to focus it so that you pass keyboard presses to it
2. Use the arrow keys to navigate the paddle
3. Press Enter to pause or resume the game
4. Press CTRL, BACKSPACE, or ESCAPE to release the mouse (and pause the game)
Note: you can enable TEST_MODE in breakout.js by setting TEST_MODE = true;. This allows the ball to bounce off the bottom of the canvas so you don't die and you can watch all the bricks get destroyed without having to play.