/** FOR TESTING TURN TEST_MODE TO TRUE **/
var TEST_MODE = true;

var canvas;

/* Paddle */
var paddle;
var PADDLE_DEFAULT_WIDTH = 100;

/* Ball */
var ball;
var BALL_DEFAULT_XSPEED = 5;
var BALL_DEFAULT_YSPEED = 8;
var hits;
var speeds = [BALL_DEFAULT_XSPEED, 8, 10, 12, 15];
var speed_index;

/* Bricks */
var bricks = []; // each array in bricks for each color
var ROWS_PER_BRICK = 2;
var BRICKS_PER_ROW = 14;
var BRICK_WIDTH = 89;
var BRICK_HEIGHT = 20;

/* Score banner */
var score;
var level;
var lives;
var stat_score;
var stat_lives;
var stat_level;
var dialog;

var toggleMouse;
var play = false;
var game_over = false;

window.onload = function() {
  canvas = document.getElementById("canvas");
  context = canvas.getContext("2d");
  toggleMouse = false;
  paddle = new Paddle(600);
  ball = new Ball(500, canvas.height - 300, 10);
  speed_index = 0;
  setBricks();
  paddle.draw();
  ball.draw();

  score = 0;
  if (TEST_MODE) {
    lives = 99;
  } else {
    lives = 3;
  }

  level = 1;
  hits = 0;

  /* Set score banner elements to update text */
  stat_score = document.getElementById("score");
  stat_lives = document.getElementById("lives");
  stat_level = document.getElementById("level");

  dialog = document.getElementById("dialog");

  dialog.innerHTML = "<p>Press ENTER to start!</p>"

  canvas.addEventListener("click", canvasClick, false);
  canvas.addEventListener("keydown", keyDown, false);
  //canvas.addEventListener("mousemove", mouseMove);

  // starts the loop
  animate();
}

/** EVENT HANDLERS **/
/* Hides mouse and puts focus on canvas */
function canvasClick(e) {
  toggleMouse = true;

  // hide cursor
  var cursorBody = document.getElementsByTagName("body")[0];
  cursorBody.style.cursor = "none";
}

/* Keyboard to pause, give input, and move paddle */
function keyDown(e) {
  var paddleSpeed = 50;
  var paddleMove;
  // if ctrl, escape, or space is pressed, release mouse
  if (e.keyCode == 17 || e.keyCode == 32 || e.keyCode == 27) {
    toggleMouse = false;
    play = false;
    dialog.innerHTML = "<p>PAUSED (mouse released) <br/> Click and press ENTER to continue</p>";
    dialog.style.display = "block";
    var cursorBody = document.getElementsByTagName("body")[0];
    cursorBody.style.cursor = "auto";
    return;
  }

  // Hitting Enter
  if (e.keyCode == 13) {
    if (game_over) {
      //reset the game
      reset();
      game_over = false;
      return;
    }

    /* if canvas is focused, pause the game */
    if (toggleMouse) {
      play = !play;
      if (play) {
        dialog.style.display = "none";
      } else {
        dialog.innerHTML = "<p>PAUSED <br/> Press ENTER to continue</p>";
        dialog.style.display = "block";
      }
    }
  }

  /* if game is being played, move paddle with arrow keys */
  if (play) {
    switch (e.keyCode) {
      case 37:
        paddle.updatePosition(paddle.x - 80);
        break;
      case 39:
        paddle.updatePosition(paddle.x + 80);
        break;
    }
  }
}

/** THAT'S A PADDLIN'**/
function Paddle(posX) {
  this.x = posX
  this.y = 488;
  this.width = PADDLE_DEFAULT_WIDTH;
  this.height = 12;
  this.shrunk = false;
}

/* Binds paddle to canvas and updates its position */
Paddle.prototype.updatePosition = function(posX) {
  if (toggleMouse) {
    // see if mouse moved left or right
    if ((posX + paddle.width) > canvas.width) {
      paddle.x = canvas.width - paddle.width;
    } else if ((posX /*- paddle.width / 2*/ ) < 0) {
      paddle.x = 0;
    } else {
      paddle.x = posX /*- (paddle.width / 2)*/ ;
    }
  } else {
    paddle.x = posX;
  }
}

/* reset to default values */
Paddle.prototype.reset = function() {
  this.x = 600
  this.y = 488;
  this.width = PADDLE_DEFAULT_WIDTH;
  this.height = 12;
  this.shrunk = false;
}

Paddle.prototype.draw = function() {
  context.beginPath();
  context.rect(this.x, this.y, this.width, this.height);
  context.fillStyle = "white";
  context.fill();
  context.closePath();
}

/** ALL KINDS OF BRICKS **/
/* Looks for bric with id equal to the parameter id and
 * removes it from the array. The function then
 * adds the brick's score value and changes speed index
 * accordingly.
 */
function removeBrick(id) {
  for (var i = 0; i < bricks.length; i++) {
    if (bricks[i].id == id) {
      score += bricks[i].score;

      hits++;

      if (hits == 4) {
        speed_index = 1;
        changeBallSpeed(speed_index);
      }

      if (hits == 12 || speed_index == 1) {
        speed_index = 2;
        changeBallSpeed(speed_index);
      }

      if (bricks[i].color == "orange" && speed_index == 2) {
        speed_index = 3;
        changeBallSpeed(speed_index);
      }

      if (bricks[i].color == "red" && speed_index == 3) {
        speed_index = 4;
        changeBallSpeed(speed_index);
      }
      bricks.splice(i, 1);
    }
  }
}

/* Brick class */
function Brick(id, x, y, color) {
  // calculate score value of brick
  var score_value;
  switch (color) {
    case "yellow":
      score_value = 1;
      break;
    case "green":
      score_value = 3;
      break;
    case "orange":
      score_value = 5;
      break;
    case "red":
      score_value = 7;
      break;
  }
  this.id = id;
  this.x = x;
  this.y = y;
  this.color = color;
  this.score = score_value;
}

/* Drawing a brick */
Brick.prototype.draw = function() {
  context.beginPath();
  context.lineWidth = "1";
  context.linecolor
  context.rect(this.x, this.y, BRICK_WIDTH, BRICK_HEIGHT);
  context.fillStyle = this.color;
  context.fill();
  context.closePath();
}

/* Draw all bricks in the array bricks */
function drawBricks() {
  for (var i = 0; i < bricks.length; i++) {
    bricks[i].draw(bricks[i].x, bricks[i].y, bricks[i].color);
  }
}

/* Sets up the bricks in the game so there are
 * 14 per row and 8 rows in the correct order
 */
function setBricks() {
  var x = 10;
  var y = 10;
  var colors = ["red", "orange", "green", "yellow"];
  var brick_id = 0;
  for (var i = 0; i < colors.length; i++) {
    var color = colors[i];
    var brick_count = 0;

    // Make two rows of each brick
    while (brick_count < BRICKS_PER_ROW * ROWS_PER_BRICK) {
      var brick = new Brick(brick_id++, x, y, color);
      bricks.push(brick);
      x += BRICK_WIDTH;

      // shift to second row
      if (brick_count == (BRICKS_PER_ROW - 1)) {
        y += BRICK_HEIGHT;
        x = 10;
      }

      brick_count++;
    }
    y += BRICK_HEIGHT;
    x = 10;
  }

  drawBricks();
}

/** BALL **/
function Ball(x, y, r) {
  this.x = x;
  this.y = y;
  this.r = r;
  this.xSpeed = BALL_DEFAULT_XSPEED;
  this.ySpeed = BALL_DEFAULT_YSPEED;
}

/* Binds ball to screen and deals with losing life
 * due to ball falling off the bottom of the canvas.
 * Also shrinks paddle if ball hits roof of canvas
 */
Ball.prototype.update = function() {
  // ball goes off bottom of screen
  if (this.y + this.ySpeed > canvas.height) {
    //die
    if (TEST_MODE) {
      this.ySpeed = -this.ySpeed;
    } else {
      this.y += this.ySpeed;
      this.y = canvas.width + 50;
      play = false;
      lives--;

      if (lives == 0) {
        dialog.innerHTML = "<p>GAME OVER <br/> Press ENTER to play again!</p>";
        game_over = true;
      } else {
        dialog.innerHTML = "<p>Ouch! You lost a life! <br/> Press ENTER to continue</p>"
        ball.reset();
        paddle.x = 600;
      }

      dialog.style.display = "block";

    }
  }

  // ball hits sides
  if (this.x + this.xSpeed > canvas.width || this.x + this.xSpeed < 0) {
    this.xSpeed = -this.xSpeed;
  }

  // ball hits roof
  if (this.y + this.ySpeed < 0) {
    this.ySpeed = -this.ySpeed;

    //shrink paddle
    if (!paddle.shrunk) {
      paddle.width = paddle.width / 2;
      paddle.shrunk = true;
    }
  }

  /* move the ball */
  this.x += this.xSpeed;
  this.y += this.ySpeed;
}


Ball.prototype.draw = function() {
  context.beginPath();
  context.arc(this.x, this.y, this.r, 0, Math.PI * 2, true);
  context.fillStyle = "white";
  context.fill();
  context.closePath();
}

/* Resets the position of the ball */
Ball.prototype.reset = function() {
  this.x = 500;
  this.y = canvas.height - 300;
  this.xSpeed = speeds[speed_index];
}

/* change ball speed for increasing difficulty */
function changeBallSpeed(speedindex) {
  if (ball.xSpeed > 0) {
    ball.xSpeed = speeds[speedindex];
  } else {
    ball.xSpeed = -(speeds[speedindex]);
  }
}

/** COLLISIONS **/
/* Collision between ball and paddle */
function checkPaddleCollisions() {
  // ball and top of paddle
  if ((ball.y >= (paddle.y - paddle.height + 10)) &&
    ((ball.x + ball.r) >= paddle.x) &&
    ((ball.x - ball.r) <= (paddle.x + paddle.width))) {
    ball.ySpeed = -(Math.abs(ball.ySpeed));

    // check if ball collides with side of paddle
    // hard coded numbers for fine-tuning
    if (ball.x + ball.r <= (paddle.x + 2)) {
      ball.xSpeed = -(Math.abs(ball.xSpeed));
    }
    if (ball.x >= (paddle.x + paddle.width - 2)) {
      ball.xSpeed = Math.abs(ball.xSpeed);
    }
  }
}

/* Collision between ball and bricks */
function checkBrickCollisions() {
  // Traverse bricks and check each of their boundries
  for (var i = 0; i < bricks.length; i++) {
    if ((ball.y >= (bricks[i].y + BRICK_HEIGHT / 2)) &&
      (ball.y <= (bricks[i].y + BRICK_HEIGHT)) &&
      ((ball.x + ball.r) >= bricks[i].x) &&
      ((ball.x - ball.r) <= (bricks[i].x + BRICK_WIDTH - 3))) {
      ball.ySpeed = Math.abs(ball.ySpeed);
      // check if ball collides with side of brick
      // hard coded numbers for fine-tuning
      if (ball.x + ball.r <= (bricks[i].x + 2)) {
        ball.xSpeed = -(Math.abs(ball.xSpeed));
      }

      if (ball.x >= (bricks[i].x + BRICK_WIDTH - 2)) {
        ball.xSpeed = Math.abs(ball.xSpeed);
      }

      // Destroy the brick if it got hit
      removeBrick(bricks[i].id);
    }
  }
}

/* Called once initial set of bricks are cleared and prepares for level 2 */
function levelUp() {
  play = false;
  level = 2;
  stat_level.innerHTML = "2";
  dialog.innerHTML = "<p>You beat Level 1! Press ENTER to start Level 2 now!</p>";
  dialog.style.display = "block";
  bricks = [];
  setBricks();
  ball.reset();
}

/* Called after the second set of bricks is cleared */
function gameWon() {
  game_over = true;
  play = false;
  dialog.innerHTML =
    "<p>Holy cows! <br/>You beat the game! <br/> Press ENTER to play again!</p>";
  dialog.style.display = "block";
}

/* Resets the level to defaults (after a game is won or lost) */
function reset() {
  bricks = [];
  setBricks();
  lives = 3;
  score = 0;
  level = 1;
  hits = 0;
  speed_index = 0;
  paddle.reset();
  ball.reset();
  dialog.innerHTML = "<p>Press ENTER to start!</p>"
  draw();
}

/* Updates the scoreboard */
function updateScores() {
  stat_score.innerHTML = score;
  stat_lives.innerHTML = lives;
  stat_level.innerHTML = level;
}

/* Checks if the score prompts for a level up or game ending win */
function checkScore() {
  if (score == 448 && level == 1) {
    levelUp();
  }

  if (score == 896) {
    gameWon();
  }
}

/** IT'S GAMETIME **/
/* Update all the things! */
function update() {
  checkScore();
  checkPaddleCollisions();
  checkBrickCollisions();
  ball.update();
  updateScores();
}

/* Draw everything in the game */
function draw() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  ball.draw();
  drawBricks();
  paddle.draw();
}

/* Main game loop */
function animate() {
  if (play) {
    update();
    draw();
  }

  window.setTimeout(animate, 1000 / 60);
}

/** FUNCTION TOOLBOX **/
/* Generates a random number between from and (to - 1) inclusive */
function randomFromTo(from, to) {
  return Math.floor(Math.random() * (to - from + 1) + from);
}