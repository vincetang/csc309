<?php
class Customer extends CI_Model {
	public $id;
	public $first;
	public $last;
	public $login;
	public $password;
	public $email;

	function __construct() {
		parent::__construct();
	}

	/** Return array of all customers stored in database */
	function getAll() {
		$query = $this->db->get('customers');
		return $query->result('Customer');
	}

	/** Return row of customer with id matching $id */
	function get($id)
	{
		$query = $this->db->get_where('customers',array('id' => $id));

		return $query->row(0,'Customer');
	}

	/** Return row of customer with login matching $login */
	function getFromLogin($login) {
		$this->db->where('login', $login);
		$this->db->limit(1);
		$query = $this->db->get('customers');

		return $query->row();
	}

	/** Remove customer with id matching $id from database */
	function delete($id) {
		$this->db->delete('customers',array('id' => $id));
	}

	/** Given an array representing a customer, insert it into
	 * the database
	 */
	function insert($customer) {
		$this->db->insert('customers', $customer);
	}

	/** Update an existing customer in a database given an
	 * array representation of a customer
	 */
	function update($customer) {
		$this->db->where('id', $customer['id']);
		$this->db->update('customers', array(
				'first' => $customer['first'],
				'last' => $customer['last'],
				'login' => $customer['login'],
				'password' => $customer['password'],
				'email' => $customer['email']));
	}

}