<?php
Class Customer_Login extends CI_Model
{
	/**
	 * Retrieves user information from database using login input
	 */
	function login($login, $password) {
		$this->db->select('id, login, password, first, last');
		$this->db->from('customers');
		$this->db->where('login', $login);
		$this->db->where('password', $password);
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
}
?>