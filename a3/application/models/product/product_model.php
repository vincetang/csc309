<?php
class Product_model extends CI_Model {

	/** Return all products in the database */
	function getAll()
	{
		$query = $this->db->get('products');
		return $query->result('Product');
	}

	/** Return the product in database with matching id */
	function get($id)
	{
		$query = $this->db->get_where('products',array('id' => $id));
		return $query->row(0,'Product');
	}

	/** Get id of product given a name */
	function getIdFromName($name) {
		$this->db->where('name', $name);
		$this->db->limit(1);
		$query = $this->db->get('products');
		return $query->row()->id;
	}

	/** Get photo from product database using id */
	function getPhotoFromId($id) {
		$this->db->where('id', $id);
		$this->db->limit(1);
		$query = $this->db->get('products');
		return $query->row()->photo_url;
	}

	/** Delete a product from the database given its id */
	function delete($id) {
		$this->db->delete('products',array('id' => $id));
	}

	/** Insert a product into the database given an array representation
	 * of the product
	 */
	function insert($product) {
		$this->db->insert('products', $product);
	}

	/** Update an existing product in the database given
	 * an array representaiton of the database
	 */
	function update($product) {
		$this->db->where('id', $product['id']);
		$this->db->update('products', array(
				'name' => $product['name'],
				'description' => $product['description'],
				'photo_url' => $product['photo_url'],
				'price' => $product['price']));
	}

}
?>