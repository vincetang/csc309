<?php
class OrderItem extends CI_Model {
	// Class attributes
	public $id;
	public $order_id;
	public $product_id;
	public $quantity;

	function __construct() {
		parent::__construct();
	}

	/** Returns all order items in the database */
	function getAll() {
		$query = $this->db->get('order_items');
		return $query->result('OrderItem');
	}

	/** Returns a single order item with id $id in the database */
	function get($id)
	{
		$query = $this->db->get_where('order_items',array('id' => $id));

		return $query->row(0,'OrderItem');
	}

	/** Deletes an existing order item with matching id */
	function delete($id) {
		$this->db->delete('order_items',array('id' => $id));
	}

	/** Insert a new order item into the database given an
	 * array representation of the order item
	 */
	function insert($orderItem) {
		$this->db->insert('order_items', $orderItem);
	}

	/** Update an existing order item in the database given
	 * an array reprsentation of the order
	 */
	function update($orderItem) {
		$this->db->where('id', $orderItem['id']);
		$this->db->update('order_items', array(
				'order_id' => $orderItem['order_id'],
				'product_id' => $orderItem['product_id'],
				'quantity' => $orderItem['quantity']));
	}
}
?>