<?php
class Order extends CI_Model {
	// Class attributes
	public $id;
	public $customer_id;
	public $order_date;
	public $order_time;
	public $total;
	public $creditcard_number;
	public $creditcard_month;
	public $creditcard_year;

	function __construct() {
		parent::__construct();
	}

	/** Returns all orders in the database */
	function getAll() {
		$query = $this->db->get('orders');
		return $query->result('Order');
	}

	/** Returns the order in the database with matching id */
	function get($id)
	{
		$query = $this->db->get_where('orders',array('id' => $id));

		return $query->row(0,'Order');
	}

	/** Delete an order from the database given its record */
	function delete($id) {
		$this->db->delete('orders',array('id' => $id));
	}

	/** Insert an order into the database given an array
	 * reprsentation of the product
	 */
	function insert($order) {
		$this->db->insert('orders', $order);
		return $this->db->insert_id();
	}

	/** Update an existing order in the database
	 * given an array representation of the order
	 */
	function update($order) {
		$this->db->where('id', $order['id']);
		$this->db->update('orders', array(
				'customer_id' => $order['customer_id'],
				'order_date' => $order['order_date'],
				'order_time' => $order['order_time'],
				'total' => $order['total'],
				'creditcard_number' => $order['creditcard_number'],
				'creditcard_month' => $order['creditcard_month'],
				'creditcard_year' => $order['creditcard_year']));
	}

}