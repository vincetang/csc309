<html>
    <head>
    <title>Edit Order</title>
    </head>

<body>
    <h1>Edit Order Details</h1>
    <!-- Form to edit an existing record in the database -->
    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/editOrder'); ?>
    <form>
        <label for="customer_id">Customer ID:</label>
        <input type="text" size="45" id="customer_id" name="customer_id" value="<?php echo $order->customer_id; ?>" />
        <br/>
        <label for="order_date">Order Date:</label>
        <input type="text" id="order_date" name="order_date" value="<?php echo $order->order_date; ?>"/>
        <br/>
        <label for="order_time">Order Time:</label>
        <input type="text" size="128" id="order_time" name="order_time" value="<?php echo $order->order_time; ?>"/>
        <br/>
        <label for="total">Total:</label>
        <input type="text" id="total" name="total" value="<?php echo $order->total; ?>"/>
        <br/>
        <label for="creditcard_number">Credit Card Number:</label>
        <input type="text" id="creditcard_number" name="creditcard_number" value="<?php echo $order->creditcard_number; ?>"/>
        <br/>
        <label for="creditcard_month">Credit Card Month:</label>
        <input type="text" id="creditcard_month" name="creditcard_month" value="<?php echo $order->creditcard_month; ?>"/>
        <br/>
        <label for="creditcard_year">Credit Card Year:</label>
        <input type="text" id="creditcard_year" name="creditcard_year" value="<?php echo $order->creditcard_year; ?>"/>
        <br/>
        <input type="hidden" name="id" id="id" value="<?php echo $order->id; ?>" />
        <input type="submit" value="Update Order"/>
    </form>
    <form action="manageOrders" method="POST">
        <input type="submit" value="Cancel"/>
    </form>
</body>
</html>
