<html>
<head>
<title>Customer Manager</title>
</head>

<body>
	<h1>Customer Manager</h1>

	<?php
	// Load a table with all customer records and buttons to delete or edit them
	echo "<table>";
	echo "<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Login</th><th>Password</th><th>E-mail</th></tr>";

	foreach ($customers as $customer) {
		echo "<tr>";
		echo "<td>" . $customer->id . "</td>";
		echo "<td>" . $customer->first . "</td>";
		echo "<td>" . $customer->last . "</td>";
		echo "<td>" . $customer->login . "</td>";
		echo "<td>" . $customer->password . "</td>";
		echo "<td>" . $customer->email . "</td>";
		echo "<td><form action='/admin/removeCustomer' method='POST'>
		<input type='hidden' id='customer_id' name='customer_id' value=" . $customer->id . " />
		<input type='submit' value='Delete' /></form></td>";
		echo "<td><form action='/admin/editCustomer' method='POST'>
		<input type='hidden' id='customer_id' name='customer_id' value=" . $customer->id . " />
		<input type='submit' value='Edit' /></form></td>";
		echo "</tr>";
	}
	echo "<table>";
	?>

	<!-- Button to remove all customers (except admins) from the database -->
	<form action="removeAllCustomers" method="POST"
		onsubmit="return confirm('WARNING! THIS WILL DELETE ALL CUSTOMER RECORDS FROM THE DATABASE, INCLUDING ORDER AND ORDER ITEM RECORDS ASSOCIATED WITH THE CUSTOMERS!')">
		<input type="submit" value="Delete all customers from database" />
	</form>

	<!--  Form to add a new user -->
	<?php echo validation_errors(); ?>
	<?php echo form_open('/admin/addCustomer'); ?>
	<h2>Add Customer</h2>
	<form>
		<label for="first">First:</label> 
		<input type="text" size="45" id="first" name="first" /> 
		<br /> 
		<label for="last">Last:</label> 
		<input type="text" id="last" name="last" /> 
		<br /> 
		<label for="login">Login:</label>
		<input type="text" size="128" id="login" name="login" /> 
		<br /> 
		<label for="password">password:</label> 
		<input type="password" id="password" name="password" /> 
		<br /> 
		<label for="email">E-mail:</label> 
		<input type="text" id="email" name="email" /> 
		<br /> 
		<input type="submit" value="Add Customer" />
	</form>
	<form action="manageCustomers" method="POST">
		<input type="submit" value="Clear" />
	</form>
	<br />
	<form action="/store" method="POST">
		<input type="submit" value="Back" />
	</form>
</body>

</html>
