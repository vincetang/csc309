<html>
    <head>
    <title>Order Items Manager</title>
    </head>

<body>
    <h1>Order Items Manager</h1>
	
	<!--  Load a table with all order items in the database and buttons to edit or delete them -->
    <?php
        echo "<table>";
        echo "<tr><th>ID</th><th>Order ID</th><th>Product ID</th><th>Quantity</th></tr>";

            foreach ($orderItems as $orderItem) {
                echo "<tr>";
                echo "<td>" . $orderItem->id . "</td>";
                echo "<td>" . $orderItem->order_id . "</td>";
                echo "<td>" . $orderItem->product_id . "</td>";
                echo "<td>" . $orderItem->quantity . "</td>";

                echo "<td><form action='/admin/removeOrderItem' method='POST'>
                <input type='hidden' id='orderItem_id' name='orderItem_id' value=" . $orderItem->id . " />
                <input type='submit' value='Delete' /></form></td>";
                echo "<td><form action='/admin/editOrderItem' method='POST'>
                <input type='hidden' id='orderItem_id' name='orderItem_id' value=" . $orderItem->id . " />
                <input type='submit' value='Edit' /></form></td>";
              
                echo "</tr>";
            }
        echo "<table>";
    ?>

    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/addOrderItem'); ?>
    <h2>Add Order</h2>
    <form>
        <label for="order_id">Order ID:</label>
        <input type="text" size="45" id="order_id" name="order_id" />
        <br/>
        <label for="product_id">Product ID:</label>
        <input type="date" id="product_id" name="product_id"/>
        <br/>
        <label for="quantity">Quantity:</label>
        <input type="time" size="128" id="quantity" name="quantity"/>
        <br/>
        <input type="submit" value="Add Order Item"/>
    </form>
    <form action="manageOrderItems" method="POST">
        <input type="submit" value="Clear"/>
    </form>
    <br/>
    <form action="/store" method="POST">
        <input type="submit" value="Back"/>
    </form>
</body>

</html>