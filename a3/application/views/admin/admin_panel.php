<!DOCTYPE html>
<html>
    <head>
        <title>Admin Control Panel</title>
    </head>

    <body>
        <h1>Admin Control panel</h1>
		<!-- The admin control panel has buttons to load views that manage the database -->
        <form action="/admin/manageCustomers" method="POST">
            <input type="submit" value="Manage Customers"/>
        </form>

        <form action="/admin/manageProducts" method="POST">
            <input type="submit" value="Manage Products"/>
        </form>

        <form action="/admin/manageOrders" method="POST">
            <input type="submit" value="Manage Orders"/>
        </form>

        <form action="/admin/manageOrderItems" method="POST">
            <input type="submit" value="Manage Order Items"/>
        </form>
    </body>
</html>