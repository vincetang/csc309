<html>
    <head>
    <title>Edit Order Item</title>
    </head>

<body>
    <h1>Edit Order Item Details</h1>
    <!-- Form to edit existing order item in the database -->
    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/editOrderItem'); ?>
   	<?php echo print_r("gothere"); ?>
   	<form>
        <label for="order_id">Order ID:</label>
        <input type="text" size="45" id="order_id" name="order_id" value="<?php echo $orderItem->order_id ?>" />
        <br/>
        <label for="product_id">Product ID:</label>
        <input type="text" id="product_id" name="product_id" value="<?php echo $orderItem->product_id ?>"/>
        <br/>
        <label for="quantity">Quantity:</label>
        <input type="text" size="128" id="quantity" name="quantity" value="<?php echo $orderItem->quantity ?>"/>
        <br/>
        <input type="hidden" name="id" id="id" value="<?php echo $orderItem->id ?>" />
        <input type="submit" value="Update Order Item"/>
    </form>
    <form action="manageOrders" method="POST">
        <input type="submit" value="Cancel"/>
    </form>
</body>
</html>
