<html>
<head>
<title>Edit Customer</title>
</head>

<body>
	<!--  Form to edit an existing customer (edit button was pushed) -->
	<h1>Edit Customer Details</h1>
	<?php echo validation_errors(); ?>
	<?php echo form_open('/admin/editCustomer'); ?>
	<form>
		<label for="first">First:</label> 
		<input type="text" size="45" id="first" name="first" value="<?php echo $customer->first ?>" /> 
		<br />
		<label for="last">Last:</label> 
		<input type="text" id="last" name="last" value="<?php echo $customer->last ?>" /> 
		<br /> 
		<label for="login">Login:</label> 
		<input type="text" size="128" id="login" name="login" value="<?php echo $customer->login ?>" /> 
		<br /> 
		<label for="password">Password:</label>
		 <input type="password" id="password" name="password" value="<?php echo $customer->password ?>" /> 
		 <br />
		 <label for="email">E-mail:</label>
		 <input type="text" id="email" name="email" value="<?php echo $customer->email ?>" /> 
		 <br /> 
		 <input type="submit" value="Update Customer" /> 
		 <input type="hidden" name="id" id="id" value="<?php echo $customer->id ?>" />
	</form>
	<form action="/admin/manageCustomers" method="POST">
		<input type="submit" value="Cancel" />
	</form>
</body>
</html>
