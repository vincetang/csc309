<html>
    <head>
    <title>Edit Product</title>
    </head>

<body>
    <h1>Edit Product Details</h1>

    <!--  form to edit existing product in the database -->
    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/editProduct'); ?>

    <form>
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="<?php echo $product->name; ?>" />
        <br/>
        <label for="description">Description:</label>
        <input type="text" id="description" name="description" value="<?php echo $product->description; ?>"/>
        <br/>
        <label for="photo_url">Photo URL:</label>
        <input type="text" id="photo_url" name="photo_url" value="<?php echo $product->photo_url; ?>"/>
        <br/>
        <label for="price">Price:</label>
        <input type="text" id="price" name="price" value="<?php echo $product->price; ?>"/>
        <br/>
        <input type="submit" value="Update Product"/>
        <input type="hidden" name="id" id="id" value="<?php echo $product->id ?>" />
    </form>
    
    <form action="/admin/manageProducts" method="POST">
        <input type="submit" value="Cancel"/>
    </form>
</body>
</html>
