<html>
    <head>
    <title>Product Manager</title>
    </head>

<body>
    <h1>Product Manager</h1>
	<!--  Load a table with all products in the database and buttons to edit or delete them -->
    <?php
        echo "<table>";
        echo "<tr><th>ID</th><th>Name</th><th>Description</th><th>Price</th><th>Photo</th></tr>";

            foreach ($products as $product) {
                echo "<tr>";
                echo "<td>" . $product->id . "</td>";
                echo "<td>" . $product->name . "</td>";
                echo "<td>" . $product->description . "</td>";
                echo "<td>" . $product->price . "</td>";
                echo "<td><img src='" . base_url() . "images/product/" . $product->photo_url . "' width='100px' /></td>";

               	echo "<td><form action='/admin/removeProduct' method='POST'>
                <input type='hidden' id='product_id' name='product_id' value=" . $product->id . " />
                <input type='submit' value='Delete' /></form></td>";
                echo "<td><form action='/admin/editProduct' method='POST'>
                <input type='hidden' id='product_id' name='product_id' value=" . $product->id . " />
                <input type='submit' value='Edit' /></form></td>";

                echo "</tr>";
            }
        echo "<table>";
    ?>

    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/addProduct'); ?>
    <h2>Add Product To Inventory</h2>
    <form>
        <label for="name">Name:</label>
        <input type="text" size="45" id="name" name="name" />
        <br/>
        <label for="description">Description:</label>
        <input type="text" id="description" name="description"/>
        <br/>
        <label for="photo_url">Photo URL:</label>
        <input type="text" size="128" id="photo_url" name="photo_url"/>
        <br/>
        <label for="price">Price:</label>
        <input type="text" id="price" name="price"/>
        <br/>
        <input type="submit" value="Add Product"/>
    </form>
    <form action="manageProducts" method="POST">
        <input type="submit" value="Clear"/>
    </form>
    <br/>
    <form action="/store" method="POST">
        <input type="submit" value="Back"/>
    </form>
</body>

</html>
