<html>
    <head>
    <title>Order Manager</title>
    </head>

<body>
    <h1>Order Manager</h1>
	
	<!--  Load a table with all orders in the database and buttons to edit or delete them -->
    <?php
        echo "<table>";
        echo "<tr><th>Order ID</th><th>Customer ID</th><th>Order Date</th><th>Order Time</th><th>Total</th><th>Credit Card Number</th><th>Credit Card Month</th><th>Credit Card Year</th></tr>";

            foreach ($orders as $order) {
                echo "<tr>";
                echo "<td>" . $order->id . "</td>";
                echo "<td>" . $order->customer_id . "</td>";
                echo "<td>" . $order->order_date . "</td>";
                echo "<td>" . $order->order_time . "</td>";
                echo "<td>" . $order->total . "</td>";
                echo "<td>" . $order->creditcard_number . "</td>";
                echo "<td>" . $order->creditcard_month . "</td>";
                echo "<td>" . $order->creditcard_year . "</td>";
                
                echo "<td><form action='/admin/removeOrder' method='POST'>
                <input type='hidden' id='order_id' name='order_id' value=" . $order->id . " />
                <input type='submit' value='Delete' /></form></td>";
                echo "<td><form action='/admin/editOrder' method='POST'>
                <input type='hidden' id='order_id' name='order_id' value=" . $order->id . " />
                <input type='submit' value='Edit' /></form></td>";
         
                echo "</tr>";
            }
        echo "<table>";
    ?>

    <?php echo validation_errors(); ?>
    <?php echo form_open('/admin/addOrder'); ?>
    <h2>Add Order</h2>
    <form>
        <label for="customer_id">Customer ID:</label>
        <input type="text" size="45" id="customer_id" name="customer_id" />
        <br/>
        <label for="order_date">Order Date:</label>
        <input type="text" id="order_date" name="order_date"/>
        <br/>
        <label for="order_time">Order Time:</label>
        <input type="text" size="20" id="order_time" name="order_time"/>
        <br/>
        <label for="total">Total:</label>
        <input type="text" id="total" name="total"/>
        <br/>
        <label for="creditcard_number">Credit Card Number:</label>
        <input type="text" id="creditcard_number" name="creditcard_number"/>
        <br/>
        <label for="creditcard_month">Credit Card Month:</label>
        <input type="text" id="creditcard_month" name="creditcard_month"/>
        <br/>
        <label for="creditcard_year">Credit Card Year:</label>
        <input type="text" id="creditcard_year" name="creditcard_year"/>
        <br/>
        <input type="submit" value="Add Order"/>
    </form>
    <form action="manageOrders" method="POST">
        <input type="submit" value="Clear"/>
    </form>
    <br/>
    <form action="/store" method="POST">
        <input type="submit" value="Back"/>
    </form>
</body>

</html>