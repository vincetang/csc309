<!DOCTYPE html>
<html>
<head>
<title>Hard Balls</title>
</head>
<body>
	<!--  Store front -->
	<h1>Hard Balls Baseball Card Shop</h1>
	<table>
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Price</th>
			<th>Photo</th>
			<?php if ($this->session->userdata('logged_in')): ?>
			<th>Purchase</th>
			<?php endif; ?>
		</tr>
		<!--  Load products in the database -->
		<?php foreach ($products as $product): ?>
		<tr>
			<td><?php echo $product->name; ?></td>
			<td><?php echo $product->description; ?></td>
			<td>$<?php echo $this->cart->format_number($product->price); ?>
			</td>
			<td><img
				src="<?php echo base_url() . 'images/product/' . $product->photo_url ?>"
				width='100px' /></td>
			<?php if ($this->session->userdata('logged_in')): ?>
			<td>
				<form action="/orders/addToCart" method="POST">
					<input type="hidden" name="id" id="id"
						value="<?php echo $product->id; ?>" /> <input type="hidden"
						name="photo_url" id="photo_url"
						value="<?php echo $product->photo_url; ?>" /> <input type="text"
						name="qty" id="qty" value="0" /> <input type="submit"
						value="Add To Cart" />
				</form>
			</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>

