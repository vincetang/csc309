<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title> Login </title>
</head>

<body>
	<!-- Form to login user -->
    <h1> Login </h1>
    <?php echo validation_errors(); ?>
    <?php echo form_open('customers/login'); ?>
    <form>
        <label for="login">Login:</label>
        <input type="text" size="16" id="login" name="login" />
        <br/>
        <label for="password">Password:</label>
        <input type="password" size="16" id="password" name="password"/>
        <br/>
        <input type="submit" value="Login"/>
        <!--<a href="customers/register">Register</a>-->
    </form>
    <?php echo form_close(); ?>
    <form action="/customers/register" method="POST">
        <input type="submit" value="Register">
    </form>
</body>
</html>
