<html>
    <head>
    <title>Customer Registration</title>
    </head>

<body>
	<!--  Form to register a new customer -->
    <?php echo validation_errors(); ?>
    <?php echo form_open('customers/register'); ?>

    <form>
        <label for="first">First Name:</label>
        <input type="text" size="24" id="first" name="first" />
        <br/>
        <label for="last">Last Name:</label>
        <input type="text" size="24" id="last" name="last"/>
        <br/>
        <label for="login">Login:</label>
        <input type="text" size="16" id="login" name="login"/>
        <br/>
        <label for="password">Password:</label>
        <input type="password" size="16" id="password" name="password"/>
        <br/>
        <label for="email">E-mail:</label>
        <input type="text" size="16" id="email" name="email"/>
        <br/>
        <input type="submit" value="Register"/>
    </form>
    <form action="/store" method="POST">
        <input type="submit" value="Cancel"/>
    </form>
</body>

</html>