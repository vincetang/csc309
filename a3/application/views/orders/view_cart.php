<!DOCTYPE html>
<html>
<head>
<title>Shopping Cart</title>
</head>

<body>
	<!--  Load the page showing the shopping cart -->
	<h1>Your Shopping Cart</h1>

	<!-- Empty cart -->
	<?php if ($this->cart->total_items() == 0): ?>
	<p>Your shopping cart is empty.</p>
	<?php else: ?>
	<!--  cart has content, display items -->
	<table>
		<h1>
			photo url:
			<?php echo $photo_url ?>
		</h1>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Picture</th>
			<th>Quantity</th>
			<th>test</th>
		</tr>
		<?php foreach ($this->cart->contents() as $item): ?>

		<tr>
			<td><?php echo $item['name'];?></td>
			<td><?php echo $item['price']; ?></td>
			<td><img
				src="<?php echo base_url() . 'images/product/' . $urls[$item['id']]; ?>"
				width='100px' /></td>
			<!-- put text fields to modify quantity -->
			<form action="/orders/updateQty" method="POST">
				<input type="hidden" name="id" value="<?php echo $item['id']; ?>" />
				<td><input type="text" name="qty"
					value="<?php echo $item['qty']; ?>" /></td>
				<td><input type="submit" value="Update Qty" /></td>
			</form>
		</tr>
		<?php endforeach; ?>
		<!--  show total cost -->
		<tr>
			<td>Total:</td>
			<td><?php echo "$" . $this->cart->format_number($this->cart->total()); ?>
			</td>
		</tr>
	</table>
	<!-- Cart Management to clear, or checkout-->
	<form action="/orders/clearCart" method="POST">
		<input type="submit" value="Clear Cart" />
	</form>

	<form action="/orders/checkout" method="POST">
		<input type="submit" value="Checkout" />
	</form>
	<?php endif; ?>

	<form action="/store" method="POST">
		<input type="submit" value="Back to store" />
	</form>
</body>
</html>
