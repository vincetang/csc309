<html>
<head>
    <title>Checkout</title>
</head>

<body>
    <?php echo validation_errors(); ?>
    <?php echo form_open('orders/checkout'); ?>

    <!-- Form during checkout to enter creditcard details -->
    <h1>Checkout</h1>
    <p>Your purchase total is: $<?php echo $this->cart->format_number($this->cart->total()); ?></p>
    <h4>Please enter your creditcard information below:</h4>
    <form>
        <label for="creditcard_number">Creditcard Number:</label>
        <input type="text" id="creditcard_number" name="creditcard_number" />
        <br/>
        <label for="creditcard_month">Month of Expiration (MM):</label>
        <input type="text" id="creditcard_month" name="creditcard_month" />
        <br/>
        <label for="creditcard_year">Year of Expiration (YY):</label>
        <input type="text" id="creditcard_year" name="creditcard_year" />
        <br/>
        <input type="submit" value="Continue Checkout" />
        </form>
</body>
</html>