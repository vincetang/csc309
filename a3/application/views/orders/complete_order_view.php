<!DOCTYPE html>
<html>
<head>
<title>Complete Order</title>
</head>

<body>
	<h1>Complete Your Order</h1>

	<!--  display cart contents to user before completing order  -->
	<table>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Qty</th>
		</tr>

		<!--  retrieve items from cart -->
		<?php foreach ($this->cart->contents() as $item): ?>
		<tr>
			<td><?php echo $item['name']; ?></td>
			<td>$<?php echo $this->cart->format_number($item['price']); ?>
			</td>
			<td><?php echo $item['qty']; ?></td>
		</tr>
		<?php endforeach; ?>

		<!--  display final total cost -->
		<tr>
			<td><p>Total:</p></td>
			<td>$<?php echo $this->cart->format_number($this->cart->total()); ?>
			</td>
		</tr>
	</table>

	<form action="/orders/completeOrder" method="POST">
		<input type="submit" value="Complete Order">
	</form>
</body>
</html>
