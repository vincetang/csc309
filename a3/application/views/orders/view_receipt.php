<!DOCTYPE html>
<html>
    <head>
        <title>Hard Balls Receipt</title>
    </head>

    <body>
    	<!--  Allow user to print receipt -->
        <button onClick="window.print()">Print Receipt</button>
        <h1>Your order has completed. Thank you for shopping at Hard Balls!</h1>
        <p>A copy of your receipt will be emailed to you shortly.</p>
        <p>Below is the receipt for your order:</p>

        <!-- General purchase and customer info -->
        <h2>Customer Receipt</h2>
        <h3>Hard Balls</h3>
        <p>Thank you <?php echo $this->session->userdata('login') ?>  for your purchase!</p>
        <br/>
        <p>Customer: <?php echo $this->session->userdata('first') . " " . $this->session->userdata('last');?></p>
        <p>Purchase Date: <?php echo $this->session->userdata('order_date'); ?></p>
        <p>Purchase Time: <?php echo $this->session->userdata('order_time'); ?></p>
        <br/>

        <!-- List purchased items -->
        <table>
            <tr><th>Name</th><th>Price</th><th>Qty</th></tr>

            <?php foreach($this->cart->contents() as $item): ?>
            <tr>
                <td><?php echo $item['name']; ?></td>
                <td>$<?php echo $this->cart->format_number($item['price']); ?></td>
                <td><?php echo $item['qty']; ?></td>
            </tr>
            <?php endforeach; ?>

            <tr>
                <td>Total:</td>
                <td>$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
            </tr>
        </table>

        <form action='/store' method="POST">
            <input type="submit" value="Back to store" />
        </form>
    </body>
</html>