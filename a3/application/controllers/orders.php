<?php
class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(array('session', 'cart', 'form_validation'));
		$this->load->helper(array('url', 'form', 'date'));
	}

	/** User pushed add to cart button. Add item associated
	 * with the button to the cart with the quantity the user
	 * entered in the input field
	 **/
	public function addToCart() {
		// If user isn't logged in, return to store front
		// Users will not see the add cart button if not logged in
		if (!$this->session->userdata('logged_in')) {
			redirect('/store');
		}

		// Get contents of cart
		$cart = $this->cart->contents();

		// ID and quantity of the item being added to cart
		$id = $this->input->post('id');
		$qty = $this->input->post('qty');

		$carted = false;

		// check if item is already in the cart
		// if it is, update the quantity with the sum of both quantities
		foreach ($cart as $item) {
			if ($item['id'] == $id) {
				$carted = true;
				$rowid = $item['rowid'];
				$qty = $item['qty'] + $qty;

				$data = array(
						'rowid' => $rowid,
						'qty' => $qty
				);

				$this->cart->update($data);
				break;
			}
		}

		// Item not found in cart, add a new entry to the cart
		if (!$carted) {
			$this->load->model('product/product_model', '', TRUE);

			$result['item'] = $this->product_model->get($id);
			$data = array(
					'id' => $id,
					'qty' => $qty,
					'price' => $result['item']->price,
					'name' => $result['item']->name
			);

			$this->cart->insert($data);
		}

		redirect('/store');
	}

	/** Empties the cart **/
	public function clearCart() {
		$this->cart->destroy();
		$this->viewCart();
	}

	/** Shows the items in the cart or a message indicating
	 * that the cart is empty.
	 * Also gives user the ability to clear the cart
	 * or modify quantities and delete items in the cart.
	 **/
	public function viewCart() {
		$this->load->model('/product/product_model', '', TRUE);

		$cart = $this->cart->contents();
		foreach ($cart as $item) {
			$photo_url = $this->product_model->getPhotoFromId($item['id']);
			$urls[$item['id']] = $photo_url;
		}

		// send photo urls back to the cart to view
		$data['urls'] = $urls;

		$this->load->view('orders/view_cart', $data);
	}

	/** Changes the quantity in the cart when a user
	 * changes the qty input field and presses update quantities
	 **/
	public function updateQty() {
		$cart = $this->cart->contents();
		$id = $this->input->post('id');
		$qty = $this->input->post('qty');

		// find item in cart with matching id
		foreach($cart as $item) {
			if ($item['id'] == $id) {
				$rowid = $item['rowid'];
			}
		}

		$newdata = array(
				'rowid' => $rowid,
				'qty' => $qty
		);

		$this->cart->update($newdata);

		$this->viewCart();
	}

	/** Opens a page that allows the user to enter their
	 * credit card information before finalizing their order
	 **/
	public function checkout() {
		$this->load->model('order', '', TRUE);

		$this->form_validation->set_rules('creditcard_number', 'Creditcard Number', 'required|exact_length[16]|is_natural|integer');
		$this->form_validation->set_rules('creditcard_month', 'Creditcard Month', 'required|is_natural_no_zero|integer|exact_length[2]|less_than[13]');
		$this->form_validation->set_rules('creditcard_year', 'Creditcard Year', 'required|is_natural_no_zero|integer|exact_length[2]');


		if ($this->form_validation->run() == FALSE) {
			$this->load->view('/orders/checkout_view');
		} else {
			if ($this->input->post('creditcard_year') < date("y") ||
					($this->input->post('creditcard_month') < date ('m') && $this->input->post('year') == date('y'))) {
				echo "<h1>Credit card is invalid</h1><br/><p>Press back to return to checkout</p>";
			} else {
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$order_info = array(
						'creditcard_number' => $this->input->post('creditcard_number'),
						'creditcard_month' => $this->input->post('creditcard_month'),
						'creditcard_year' => $this->input->post('creditcard_year'),
						'order_date' => $date,
						'order_time' => $time
				);

				$this->session->set_userdata($order_info);
				$this->load->view('/orders/complete_order_view');
			}
		}
	}

	/** Last step in the checkout process where the user
	 * finalizes their order and confirms the purchase.
	 * This function also emails a receipt once the order is
	 * confirmed and has a button which allows the user
	 * to print their receipt.
	 **/
	 
	public function completeOrder() {
		// Retrieve customer details
		$this->load->model('/login/customer', '', TRUE);

		$login = $this->session->userdata('login');
		$cust['customer'] = $this->customer->getFromLogin($login);
		$customer_id = $cust['customer']->id;

		// Prepare order record to insert into database
		$date = date('Y-m-d');
		$time = date('h:i:s');

		$order_record = array(
				'customer_id' => $customer_id,
				'order_date' => $this->session->userdata('order_date'),
				'order_time' => $this->session->userdata('order_time'),
				'total' => $this->cart->total(),
				'creditcard_number' => $this->session->userdata('creditcard_number'),
				'creditcard_month' => $this->session->userdata('creditcard_month'),
				'creditcard_year' => $this->session->userdata('creditcard_year')
		);

		// insert into the database and get the order id
		$this->load->model('order', '', TRUE);
		$order_id = $this->order->insert($order_record);

		// Prepare order item record to insert into database
		$this->load->model('orderitem', '', TRUE);
		$this->load->model('/product/product_model', '', TRUE);

		// Insert each ordered item into the database
		foreach ($this->cart->contents() as $item) {
			$product_id = $this->product_model->getIdFromName($item['name']);
			$order_item_record = array(
					'order_id' => $order_id,
					'product_id' => $product_id,
					'quantity' => $item['qty']
			);

			$this->orderitem->insert($order_item_record);
		}

		// Set email configuration
		$email_config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'vincenttangcsc309@gmail.com',
				'smtp_pass' => 'amd12345',
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1',
				'newline' => "\r\n"
		);
		$this->load->library('email', $email_config);

		//email the receipt to the user using their account email
		$this->email->from('vinceStore@gmail.com', 'Hard Balls');
		$this->email->to($cust['customer']->email);
		$this->email->subject('Receipt for your recent purchase at Hard Balls');
		$content = $this->load->view('/orders/view_receipt', '', TRUE);
		$this->email->message($content);
		$this->email->send();

		$this->viewReceipt();

		// Empty the cart
		$this->cart->destroy();
	}

	/** Loads the receipt page **/
	public function viewReceipt() {
		$this->load->view('/orders/view_receipt');
	}
}
?>