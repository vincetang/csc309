<?php

class Store extends CI_Controller {

	/**
	 * Main entry point for the store
	 *
	 * Maps to the following URL
	 * 		http://localhost
	 **/
	public function index()
	{
		$this->load->helper(array('url','form'));
		$this->load->library(array('session', 'cart'));
		// load login options
		if (!$this->session->userdata('logged_in')) {

			$this->load->view('login/login_view');
		} else {
			// logged in

			// view cart button
			$this->load->view('orders/view_cart_button');

			// show logout button
			$this->load->view('login/logout');

			// User is an admin so we load the control panel
			if ($this->session->userdata('login') == "admin") {
				$this->load->view('admin/admin_panel');
			}
		}
		$this->load->model('product/product_model');

		// Get all the products from the database
		$products = $this->product_model->getAll();
		$data['products']=$products;

		// Load the store products
		$this->load->view('/store/store_products.php', $data);
	}
}
?>