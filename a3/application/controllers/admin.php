<?php
class Admin extends CI_Controller {

	/** Loads up the view to manage products which includes
	 * a list of the current inventory and a form to add
	 * new products
	 **/
	public function manageProducts() {
		// show a view with a form to add products
		// and a list of products to delete
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// Show view that lists products and form to add products
		$this->addProduct();
	}

	/** Loads the view to list inventory and
	 * has a form to add products
	 **/
	public function addProduct() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// load form validation rules
		$this->form_validation->set_rules('name', 'Product name', 'required');
		$this->form_validation->set_rules('description', 'Product description', 'required');
		$this->form_validation->set_rules('photo_url', 'Photo URL', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');

		if ($this->form_validation->run() == FALSE) {
			// product cannot be added because form was not filled properly
			$this->load->model('product/product_model');

			// Get all the products from the database to re-list inventory
			$products = $this->product_model->getAll();
			$data['products']=$products;

			$this->load->view('/admin/admin_product_view',$data);
		} else {
			// product form validated successfully and add item
			// was pressed. Create data for new item to be added
			// using form data
			$newProduct = array (
					'id' => 0,
					'name' => $this->input->post('name'),
					'description' => $this->input->post('description'),
					'photo_url' => $this->input->post('photo_url'),
					'price' => $this->input->post('price')
			);

			$this->load->model('/product/product_model', '', TRUE);

			$this->product_model->insert($newProduct);

			// Refresh the product CP again
			redirect('/admin/manageProducts');
		}
	}

	/** Removes the product this button is associated with **/
	public function removeProduct() {
		$this->load->model('product/product_model');
		$id = $this->input->post('product_id');
		// delete the product with the specified id
		$this->product_model->delete($id);

		// Reload product manager
		$this->manageProducts();
	}

	/** Edit the product this button is associated with **/
	public function editProduct() {
		// load required assets
		$this->load->model('product/product_model', '', TRUE);
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// get id of product being modified from POST
		$id = $this->input->post('product_id');

		// Set form rules for new details
		$this->form_validation->set_rules('name', 'Product name', 'required');
		$this->form_validation->set_rules('description', 'Product description', 'required');
		$this->form_validation->set_rules('photo_url', 'Photo URL', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');

		// Retrieve the item  being modified from the DB
		$row = $this->product_model->get($id);
		$data = array('product' => $row);
		if ($this->form_validation->run() == FALSE) {
			// product cannot be added, load the view again
			$this->load->view('/admin/admin_edit_product_view', $data);
		} else {
			// product form validated successfully, update the DB
			$updateProduct = array (
					'id' => $this->input->post('id'),
					'name' => $this->input->post('name'),
					'description' => $this->input->post('description'),
					'photo_url' => $this->input->post('photo_url'),
					'price' => $this->input->post('price')
			);
			$this->product_model->update($updateProduct);

			redirect('/admin/manageProducts');
		}
	}

	/** Creates a view and form to manage customers as an admin **/
	public function manageCustomers() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// Show form to add and view customers
		$this->addCustomer();
	}

	/** Lists all the customers in the DB and allows admin to
	 * add a new customer by filling in the form
	 **/
	public function addCustomer() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// Set form rules to add a new customer
		$this->form_validation->set_rules('first', 'First name', 'trim|required');
		$this->form_validation->set_rules('last', 'Last name', 'trim|required');
		$this->form_validation->set_rules('login', 'Login', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

		if ($this->form_validation->run() == FALSE) {
			$this->load->model('login/customer', '', TRUE);

			// Form validation failed, re-list customers in DB
			$customers = $this->customer->getAll();
			$data['customers']=$customers;
			$this->load->view('/admin/admin_customer_view',$data);
		} else {
			// product form validated successfully
			// Create new customer data using form data
			$newCustomer = array (
					'id' => 0,
					'first' => $this->input->post('first'),
					'last' => $this->input->post('last'),
					'login' => $this->input->post('login'),
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email')
			);
			$this->load->model('login/customer', '', TRUE);

			$this->customer->insert($newCustomer);

			// Refresh the product CP again
			redirect('/admin/manageCustomers');
		}
	}

	/** Remove customer associated with this delete button **/
	public function removeCustomer() {
		$this->load->model('login/customer', '', TRUE);
		$id = $this->input->post('customer_id');
		$this->customer->delete($id);
		$this->manageCustomers();
	}

	/** Remove all customers except admins from the database **/
	public function removeAllCustomers() {
		$this->load->model('login/customer', '', TRUE);

		$result = $this->customer->getAll();

		foreach ($result as $cust)  {
			// Check that the user is not an admin
			if ($cust->login != 'admin') {
				$id = $cust->id;
				$this->customer->delete($id);
				
				// Get order ID's associated with customer
				$query = $this->db->get_where('orders', array('customer_id' => $id));
				
				foreach ($query->result() as  $row) {
					$order_id = $row->order_id;
					
					$this->db->where('order_id', $order_id);
					$this->db->delete('order_items');
				}
				// delete all orders and order items associated with the customer
				$this->db->where('customer_id', $id);
				$this->db->delete('orders');
			}
		};

		$this->manageCustomers();
	}

	/** Edit the customer associated with the edit button, using a form **/
	public function editCustomer() {
		// load assets
		$this->load->model('login/customer', '', TRUE);
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// get customer id from POST
		$id = $this->input->post('customer_id');

		// set form rules
		$this->form_validation->set_rules('first', 'First name', 'trim|required');
		$this->form_validation->set_rules('last', 'Last name', 'trim|required');
		$this->form_validation->set_rules('login', 'Login', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

		// retreive all customers to display
		$row = $this->customer->get($id);
		$data = array('customer' => $row);
		if ($this->form_validation->run() == FALSE) {

			// form validation failed, reload view with customer list
			$this->load->view('/admin/admin_edit_customer_view', $data);
		} else {
			// customer form validated successfully, use form to create data
			$updateCustomer = array (
					'id' => $this->input->post('id'),
					'first' => $this->input->post('first'),
					'last' => $this->input->post('last'),
					'login' => $this->input->post('login'),
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email')
			);

			$this->customer->update($updateCustomer);

			redirect('/admin/manageCustomers');
		}
	}

	/** Load admin panel for orders **/
	public function manageOrders() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->addOrder();
	}

	/** Form to add orders and lists orders **/
	public function addOrder() {
		// load assets
		$this->load->helper(array('form', 'url','date'));
		$this->load->library('form_validation');

		// load form rules
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'trim|required');
		$this->form_validation->set_rules('order_date', 'Order Date', 'trim|required');
		$this->form_validation->set_rules('order_time', 'Order Time', 'trim|required');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|numeric');
		$this->form_validation->set_rules('creditcard_number', 'Credit Card Number', 'trim|required|exact_length[16]');
		$this->form_validation->set_rules('creditcard_month', 'Credit Card Expiry Month', 'trim|required|numeric|exact_length[2]');
		$this->form_validation->set_rules('creditcard_year', 'Credit Card Expiry Year', 'trim|required|numeric|exact_length[2]');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed, reload orders list
			$this->load->model('order', '', TRUE);
			$orders = $this->order->getAll();
			$data['orders'] = $orders;
			$this->load->view('/admin/admin_order_view',$data);
		} else {
			// product form validated successfully

			//check if customer ID exists
			$this->load->model('/login/customer', '', TRUE);
			if (count(($this->customer->get($this->input->post('customer_id')))) == 0) {
				// no customer ID found
				echo "<h2>Invalid Customer ID</h2><br/><p>Press back to return to Order Manager</p>";
			} else {
				// customer exists, create the order
				$newOrder = array (
						'id' => 0,
						'customer_id' => $this->input->post('customer_id'),
						'order_date' => $this->input->post('order_date'),
						'order_time' => $this->input->post('order_time'),
						'total' => $this->input->post('total'),
						'creditcard_number' => $this->input->post('creditcard_number'),
						'creditcard_month' => $this->input->post('creditcard_month'),
						'creditcard_year' => $this->input->post('creditcard_year')
				);
				$this->load->model('order', '', TRUE);
				$this->order->insert($newOrder);

				// Refresh the product CP again
				redirect('/admin/manageOrders');
			}
		}
	}

	/** function to modify order associated with edit button **/
	public function editOrder() {
		$this->load->model('order', '', TRUE);
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$id = $this->input->post('order_id');

		$this->form_validation->set_rules('customer_id', 'Customer ID', 'trim|required');
		$this->form_validation->set_rules('order_date', 'Order Date', 'trim|required');
		$this->form_validation->set_rules('order_time', 'Order Time', 'trim|required');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|numeric');
		$this->form_validation->set_rules('creditcard_number', 'Credit Card Number', 'required|exact_length[16]');
		$this->form_validation->set_rules('creditcard_month', 'Credit Card Expiry Month', 'required|numeric|exact_length[2]');
		$this->form_validation->set_rules('creditcard_year', 'Credit Card Expiry Year', 'required|numeric|exact_length[2]');

		$row = $this->order->get($id);
		$data = array('order' => $row);
		if ($this->form_validation->run() == FALSE) {
			// form validation failed
			$this->load->view('/admin/admin_edit_order_view', $data);
		} else {
			// product form validated successfully
			$updateOrder = array (
					'id' => $this->input->post('id'),
					'customer_id' => $this->input->post('customer_id'),
					'order_date' => $this->input->post('order_date'),
					'order_time' => $this->input->post('order_time'),
					'total' => $this->input->post('total'),
					'creditcard_number' => $this->input->post('creditcard_number'),
					'creditcard_month' => $this->input->post('creditcard_month'),
					'creditcard_year' => $this->input->post('creditcard_year')
			);
			$this->order->update($updateOrder);

			redirect('/admin/manageOrders');
		}
	}

	/** Remove order associated with delete button **/
	public function removeOrder() {
		$id = $this->input->post('order_id');
		$this->load->model('order', '', TRUE);

		$this->order->delete($id);
		
		//Delete associated order items
		$this->db->where('order_id', $id);
		$this->db->delete('order_items');

		// Reload product manager
		$this->manageOrders();
	}

	/** Admin CP for order Items **/
	public function manageOrderItems() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// Show form to manage orders
		$this->addOrderItem();
	}

	/** Add order item to database **/
	public function addOrderItem() {

		$this->load->helper(array('form', 'url','date'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('order_id', 'Order ID', 'required');
		$this->form_validation->set_rules('product_id', 'Product ID', 'required');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required');

		if ($this->form_validation->run() == FALSE) {
			// Form validation failed
			$this->load->model('orderitem', '', TRUE);

			// Get all the order items from the database to list
			$orderItems = $this->orderitem->getAll();
			$data['orderItems']=$orderItems;

			$this->load->view('/admin/admin_orderitem_view',$data);
		} else {
			// product form validated successfully

			$newOrderItem = array (
					'id' => 0,
					'order_id' => $this->input->post('order_id'),
					'product_id' => $this->input->post('product_id'),
					'quantity' => $this->input->post('quantity')
			);
			$this->load->model('orderitem', '', TRUE);
			$this->orderitem->insert($newOrderItem);
			// Refresh the product CP again
			redirect('/admin/manageOrderItems');
		}
	}

	/** Remove orderItem associated with delete button **/
	public function removeOrderItem() {
		$id = $this->input->post('orderItem_id');
		$this->load->model('orderitem', '', TRUE);

		// delete the product with the specified id
		$this->orderitem->delete($id);

		// Reload manager
		$this->manageOrderItems();
	}

	/** Modify order item associated with edit button **/
	public function editOrderItem() {
		// load assets
		$this->load->model('orderitem', '', TRUE);
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$id = $this->input->post('orderItem_id');

		// set form rules
		$this->form_validation->set_rules('order_id', 'Order ID', 'trim|required');
		$this->form_validation->set_rules('product_id', 'Product ID', 'trim|required');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');

		// query data using id
		$row = $this->orderitem->get($id);
		$data = array('orderItem' => $row);
		if ($this->form_validation->run() == FALSE) {
			// form validation failed
			$this->load->view('/admin/admin_edit_orderitem_view', $data);
		} else {
			// product form validated successfully, update order item
			$updateOrderItem = array (
					'id' => $this->input->post('id'),
					'order_id' => $this->input->post('order_id'),
					'product_id' => $this->input->post('product_id'),
					'quantity' => $this->input->post('quantity'),
			);
			$this->orderitem->update($updateOrderItem);

			redirect('/admin/manageOrderItems');
		}
	}
}

?>