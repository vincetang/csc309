<?php
class Customers extends CI_Controller {

	/** Loads the page with login window **/
	public function index() {
		$this->load->view('login/login_view');
	}

	/** Logout button pressed, destroy user data. **/
	public function logout() {
		$this->load->library('session');
		$this->load->helper('url');


		$this->session->sess_destroy();

		// Return to store front
		$this->load->helper('url');
		redirect('/');
	}

	/** Load the register form and register a new user when
	 * the register button is pressed
	 **/
	public function register() {
		$this->load->helper(array('form','url'));
		$this->load->library(array('form_validation', 'session'));
		$this->load->model('login/customer', '', TRUE);

		// form_rules
		$this->form_validation->set_rules('first', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('login', 'Login', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

		if ($this->form_validation->run() == FALSE) {
			// Registration failed, form not filled properly
			$this->load->view('login/register');
		} else {
			// Registration fields filled properly
			$data = array(
					'first' => $this->input->post('first'),
					'last' => $this->input->post('last'),
					'login' => $this->input->post('login'),
					'password' => $this->input->post('password'),
					'email' => $this->input->post('email')
			);

			$this->customer->insert($data);

			// set session data to the new user (automically logs them in)
			$sessiondata = array(
					'login' => $this->input->post('login'),
					'first' => $this->input->post('first'),
					'last' => $this->input->post('last'),
					'logged_in' => TRUE
			);
			$this->session->set_userdata($sessiondata);

			// back to store front
			redirect('/');
		}
	}

	/** User presses login button. Validates login information
	 * and sets userdata if the user is registered and logins
	 * successfully.
	 **/
	function login() {
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));

		$this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[16]|xss_clean|callback_check_database');

		if ($this->form_validation->run() == FALSE) {
			// Field validation failed. Return to login page with validation messages
			$this->load->view('login/login_view');
			$this->load->view('login/failed_login_view');
		} else {
			// Logged in, go back to store front
			redirect('/');
		}
	}

	/** This function is called if field validation during login is successful.
	 * Checks the login info against database
	 **/
	function check_database($password) {
		$this->load->model('login/customer_login', '', TRUE);

		$login = $this->input->post('login');

		// get information of customer logging in
		$result = $this->customer_login->login($login, $password);

		if ($result) {
			// A user in the database matches login details. Set userdata.
			$sess_array = array();
			foreach ($result as $row) {
				$sess_array = array(
						'id' => $row->id,
						'login' => $row->login,
						'first' => $row->first,
						'last' => $row->last,
						'logged_in' => TRUE
				);
				$this->session->set_userdata($sess_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return FALSE;
		}
	}
}
?>