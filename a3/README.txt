===================
CSC309 Assignment 3
===================

Vincent Tang
g2tangvi
998192450

---
AMI
---
    Name: vincenttang_csc309_a3_final
    AMI ID: ami-16f9697e

----------------
Source Locations
----------------
    Application: '/var/www/html/application'
    Controllers: '/var/www/html/application/controllers'
    Models: '/var/www/html/application/models'
    Views: '/var/www/html/application/views'

    Entry point: '/var/www/html/application/controllers/store.php'
    Store images in: '/var/www/html/images/product'

----------
How to use
----------
1. Open browser (Firefox or Chrome)
2. Enter "localhost" into URL (note there is no application name following the base url)
3. To login as admin, use the login 'admin' and password 'password'

-----------------
Addtional Details
-----------------
> Add to cart button does not load until a user logs in
> Creditcard expiry is validated when checking out, however an admin
 can manually add expired credit cards to the database
> Admins are given special permissions to modfy the database. Any user with the login 'admin' is considered an admin which allows the admin control panel to be visible.